 
require("curses");
require("awesome");
require("public");

locale="en"

NORMAL=1
SELECTED=2
NO_RESULTS=3
SEARCHBOX=4

--Initialize curses
curses.initscr(); --Initialize the curses library
curses.cbreak();  --Disable Line Buffering
curses.keypad(curses.stdscr(), true); --Makes getch work correctly
curses.noecho(); --Does not print back any input
curses.start_color(); --Allow colors

--Initialize colors
curses.init_pair(NORMAL, curses.COLOR_BLUE, curses.COLOR_YELLOW);
curses.init_pair(SELECTED, curses.COLOR_CYAN, curses.COLOR_RED);
curses.init_pair(NO_RESULTS, curses.COLOR_RED, curses.COLOR_CYAN);
curses.init_pair(SEARCHBOX, curses.COLOR_BLACK, curses.COLOR_WHITE);

--Set up mouse
-- curses.addmousemask(curses.BUTTON1_CLICKED);

apps = quicklaunchbar(case_sensitive)
current = 1
top_of_screen = 1
search = nil

function set_search(term)
    curses.move(0,0)
    curses.attrset(curses.COLOR_PAIR(SEARCHBOX));
    curses.addstr(string.rep(" ", curses.COLS()));
    if term then
	curses.move(0,0)
	curses.addstr(search);
    end
end

function set_current(style)
    curses.move(current-top_of_screen+1,0)
    curses.attrset(curses.COLOR_PAIR(style));
    curses.clrtoeol();
    curses.addstr(apps[current]['name'])
end

function draw_menu(start) --i == starting number
    i=1
    while i < curses.LINES() do
	curses.move(i, 0);
	curses.clrtoeol();
	if start > table.getn(apps) then break end
	if start == current then
	    curses.attrset(curses.COLOR_PAIR(SELECTED));
	else
	    curses.attrset(curses.COLOR_PAIR(NORMAL));
	end
	curses.addstr(apps[start]['name']);
	i = i + 1;
	start = start+1;
    end
    set_search(search);
    if search==nil then
	curses.move(current-top_of_screen+1,string.len(apps[current].name));
    end
end
draw_menu(1);


function show_message(name, version, comment, exec)
    local width = string.len(message) + 6;
    win = curses.newwin(5, width, (curses.LINES() - 5) / 2, (curses.COLS() - width) / 2);
    win:box('|', '-');
    win:mvaddstr(2, 3, message);
    win:getch();
    win:delwin();
end

--Searches a table for something that starts with an element.
-- Being a bisection search, it is guaranteed to run in precisely O(log n)
--NOTE: To use this with any regular table, just remove all the references to ["name"]
function bisearch(haystack, needle, a, b)
    a = a or 1;
    b = b or table.getn(haystack);
    if a==b then 
	if (not case_sensitive
	   and string.sub(haystack[a]["name"], 1, string.len(needle)):upper()~=needle:upper())
	   or (string.sub(haystack[a]["name"], 1, string.len(needle))~=needle) then
	    a=-1;
	end
	return a
    end
    c = a + math.floor((b-a)/2);
    if not case_sensitive and needle:upper() > haystack[c]["name"]:upper() then
	return bisearch(haystack, needle, c+1, b)
    elseif case_sensitive and needle > haystack[c]["name"] then
	return bisearch(haystack, needle, c+1, b)
    else
	return bisearch(haystack, needle, a, c)
    end
end




while true do
    key = curses.getch();
--     if(true) then break end
    --------------Entering a search / Alphanumeric and Backspace--------------
    if search~=nil and (key==127 or key==string.byte(' ') --Backspace & Space
	or (key>=string.byte('a') and key<=string.byte('z'))
	or (key>=string.byte('A') and key<=string.byte('Z'))
	or (key>=string.byte('0') and key<=string.byte('9'))) then
	--------------Backspace--------------
	if key==127 then
	    if string.len(search)>0 then
		search = string.sub(search, 1, -2)
		set_search(search);
	    end
	--------------Alphanumeric--------------
	else
	    search = search .. string.char(key);
	    res = bisearch(apps, search);
	    --No result found
	    if res<0 then
		set_current(NO_RESULTS);
		set_search(search);
	    --Result found
	    else
		current = res;
		top_of_screen = current - 5;
		if top_of_screen<1 then top_of_screen=1 end
		draw_menu(top_of_screen)
	    end
	end
    --------------Quitting the program--------------
    elseif key==string.byte('q') or key==string.byte('Q') then
	break
    --------------Initiating a search--------------
    elseif key==string.byte('s') or key==string.byte('S') then
	set_current(NORMAL);
	curses.move(0,0)
	search = ""
    --------------Adjusting for resize--------------
    elseif(key==curses.KEY_RESIZE) then
	draw_menu(top_of_screen);
    --~~~~~~~~~~~~Things that will reset the search box~~~~~~~~~~~~--
    else
	search=nil
	set_search(search);
	curses.move(current, string.len(apps[current].name))
	--------------Scroll up--------------
	if(key==curses.KEY_UP and current>1) then
	    --Deselect last
	    set_current(NORMAL)
	    --Decrement
	    current = current - 1;
	    --Select current
	    if (current - top_of_screen) < 0 then
		top_of_screen = top_of_screen-1;
		draw_menu(top_of_screen);
	    else
		set_current(SELECTED)
	    end
	--------------Scroll down--------------
	elseif(key==curses.KEY_DOWN and current<table.getn(apps)) then
	    --Deselect last
	    set_current(NORMAL);
	    --Increment
	    current = current + 1;
	    --Select Current
	    if (current - top_of_screen + 1) == curses.LINES() then
		top_of_screen = top_of_screen+1;
		draw_menu(top_of_screen);
	    else
		set_current(SELECTED);
	    end
	--------------Page Up--------------
	elseif(key==339 and top_of_screen>1) then
	    top_of_screen = top_of_screen - curses.LINES() - 1;
	    current = current - curses.LINES() - 1;
	    if top_of_screen<1 then top_of_screen = 1 end
	    if current<1 then current = 1 end
	    draw_menu(top_of_screen);
	--------------Page Down--------------
	elseif(key==338 and top_of_screen<table.getn(apps)) then
	    top_of_screen = top_of_screen + curses.LINES() - 1;
	    current = current + curses.LINES() - 1;
	    if top_of_screen>table.getn(apps) then top_of_screen = table.getn(apps) end
	    if current>table.getn(apps) then current = table.getn(apps) end
	    draw_menu(top_of_screen);
	--------------Execute Program--------------
	elseif(key==curses.KEY_ENTER or key==10) then
	    os.execute(apps[current]['command'] .. "&2>/dev/null &");
	--------------Mouse WIP--------------
	elseif(key==curses.KEY_MOUSE) then
	    local r, id, x, y, z, bstate = curses.getmouse();
	    key=bstate
	    if(key==bstate) then break end
	    if(r and bstate==curses.BUTTON1_CLICKED) then
		--User clicked search box
		if(y==0) then
		    set_current(NORMAL);
		    curses.move(0,0)
		    search = ""
		--User clicked entry
		elseif(x-1<=string.len(apps[current]['name'])) then
		    set_current(NORMAL);
		    current = top_of_screen + y - 1;
		    set_current(SELECTED);
		    os.execute(apps[current]['command'] .. "&2>/dev/null &");
		end    
	    end
	else
	    set_current(SELECTED);
	end
    end
end
curses.endwin();

